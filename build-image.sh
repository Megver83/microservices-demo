#!/bin/sh
# TODO: Reemplazar con skaffold https://skaffold.dev/docs/tutorials/ci_cd/#step-9-getting-the-ci-pipeline-setup
set -e

registry_image="$CI_REGISTRY_IMAGE"
tag=latest
push=false

while getopts "r:pt:h" opt; do
  case "$opt" in
    r)
      registry_image="$OPTARG"
    ;;
    p)
      push=true
    ;;
    t)
      tag="$OPTARG"
    ;;
    h)
      echo "Usage: ${0##*/} [-p] [-t TAG] -r [REGISTRY_IMAGE] DIRECTORY[:IMAGE_NAME] [DIRECTORY2] ..."
      exit 0
    ;;
    *)
      echo "Invalid option, run with '-h' to see the help"
      exit 1
    ;;
  esac
done

shift $(( OPTIND - 1 ))

if ! test "${#registry_image}" -gt 0; then
  echo "Please specify a registry with '-r' or through CI_REGISTRY_IMAGE env var"
  exit 1
fi

for dir_image in "$@"; do
  if echo "$dir_image" | grep -q ":"; then
    image_name="${dir_image##*:}"
    dir="${dir_image%:*}"
  else
    image_name="${dir_image##*/}"
    dir="$dir_image"
  fi
  cd "$dir"

  docker build -t "$registry_image/$image_name:$tag" .
  if "$push"; then
    docker push "$registry_image/$image_name:$tag"
  fi

  cd -
done
